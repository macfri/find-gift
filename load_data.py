import settings

from sqlalchemy import create_engine, orm
from models import Gift

engine = create_engine(settings.DATABASE_DSN)
Session = orm.sessionmaker(bind=engine)()

data = [
    dict(
        title='carro',
        price=78,
        sexo='male',
        age=15,
        category='mujer',
        keywords=['auto', 'toyota', 'rueda'],
        sku='20001',
        gender='male'
    ),
    dict(
        title='pato',
        price=12,
        sexo='male',
        age=15,
        category='juvenil',
        keywords=['pato', 'animal', 'amarillo'],
        sku='20002',
        gender='male'
    ),
    dict(
        title='silla',
        price=78,
        sexo='male',
        age=40,
        category='hombre',
        keywords=['silla', 'madera'],
        sku='20003',
        gender='male'
    ),
    dict(
        title='guitarra',
        price=78,
        sexo='female',
        age=15,
        category='oportunidades',
        keywords=['guitarra', 'electrica'],
        sku='20004',
        gender='female'
    ),
    dict(
        title='twitter',
        price=78,
        sexo='female',
        age=15,
        category='oportunidades',
        keywords=['twitter', 'electrica'],
        sku='20005',
        gender='female'
    ),
    dict(
        title='skype',
        price=78,
        sexo='female',
        age=15,
        category='oportunidades',
        keywords=['sdsd', 'skype'],
        sku='20006',
        gender='male'
    ),
    dict(
        title='web',
        price=78,
        sexo='female',
        age=15,
        category='oportunidades',
        keywords=['web2', 'web'],
        sku='20007',
        gender='male'
    ),
    dict(
        title='teatro',
        price=78,
        sexo='female',
        age=15,
        category='oportunidades',
        keywords=['teatro', 'peaple'],
        sku='20008',
        gender='female'
    ),
    ]

for x in data:
    gift = Gift()
    gift.title = x.get('title')
    gift.sku = x.get('sku')
    gift.age = x.get('age')
    gift.keywords = ','.join(x.get('keywords'))
    gift.price = x.get('price')
    gift.category = x.get('category')
    gift.stores = 'a, b, c'
    gift.gender = x.get('gender')

    Session.add(gift)

Session.commit()
