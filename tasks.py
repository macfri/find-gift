import settings
import urllib
import time

from celery import Celery
from celery.utils import LOG_LEVELS

from pymongo import Connection

import logging
import simplejson

celery = Celery(
     'tasks',
    broker=settings.BROKER_URL
)

#celery.conf.CELERY_TASK_SERIALIZER = 'json'
celery.conf.CELERYD_LOG_LEVEL = LOG_LEVELS['INFO']

cn = Connection()
db = cn.tags


@celery.task
def tags(user_id, connector, friend_id, access_token, url=None):

    if not url:

        params = dict(
            access_token=access_token,
            fields='name',
            limit=50
        )

        url = '%s/%s/%s?%s' % (
            settings.FACEBOOK_GRAPH,
            friend_id,
            connector,
            urllib.urlencode(params)
        )

    logging.info('url: %s' % url)

    try:
        data = simplejson.loads(urllib.urlopen(url).read())
    except IOError as exc:
        logging.info(exc)
        time.sleep(5)
        tags.retry(user_id, connector, friend_id, access_token)

    items = data.get('data')

    user = db.items.find_one({'user_id': user_id})

    if items:

        for x in items:

            keywords = x['name'].lower().split(' ')

            if not user:

                db.items.insert(dict(
                    user_id=user_id,
                    keywords=keywords
                ))

            else:

                db.items.update(
                    {'user_id': user_id},
                    {'$push': {'keywords': keywords}},
                )

        paging = data.get('paging')

        if 'next' in paging:
            tags.delay(
                user_id,
                connector,
                friend_id, access_token, paging.get('next')
            )
