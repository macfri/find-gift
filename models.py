from datetime import datetime

from sqlalchemy import Column, ForeignKey, text
from sqlalchemy.types import Integer, Unicode, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class User(Base):

    __tablename__ = "users"
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    email = Column(Unicode(50), unique=True, nullable=False)
    fbid = Column(Unicode(100), unique=True, nullable=False)
    gifts = relationship('UserGift', backref='user')
    created_at = Column(DateTime, default=datetime.now)


class Gift(Base):

    __tablename__ = "gifts"
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(200), nullable=False)
    sku = Column(Unicode(200), nullable=False)
    price = Column(Integer, nullable=False)
    age = Column(Integer, nullable=False)
    stores = Column(Unicode(1000), nullable=False)
    keywords = Column(Unicode(1000), nullable=False)
    category = Column(Unicode(200), nullable=False)
    gender = Column(Unicode(200), nullable=False)
    enabled = Column(
        Boolean,
        nullable=False,
        server_default=text("1"),
        default=True
    )
    created_at = Column(DateTime, default=datetime.now)


class UserGift(Base):

    __tablename__ = "user_gifts"
    __table_args__ = {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8'}

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer,
        ForeignKey("users.id", ondelete='cascade'), nullable=False)
    gift_id = Column(Integer,
        ForeignKey("gifts.id", ondelete='cascade'), nullable=False)
    friend_id = Column(Unicode(100), nullable=False)
    gift = relationship(Gift, backref='user_gifts')


if __name__ == "__main__":

    import settings
    from sqlalchemy import create_engine

    engine = create_engine(settings.DATABASE_DSN)
    Base.metadata.create_all(engine)
