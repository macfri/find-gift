import os

from jinja2 import Environment, FileSystemLoader

DEBUG = True
XSRF_COOKIES = False
COOKIE_SECRET = '2Ykh63389Hcxd2gbQ5c97n9u7u58R913'

DATABASE_DSN = "mysql://root:root@localhost/saga_gifts"

STATIC_PATH = os.path.join(os.path.dirname(__file__), "static")
STATIC_URL_PREFIX = "/static/"

TEMPLATE_PATH = os.path.join(os.path.dirname(__file__), 'templates')
TEMPLATE_ENV = Environment(loader=FileSystemLoader(TEMPLATE_PATH))

BROKER_URL = "mongodb://localhost:27017/tags"

#FACEBOOK_GRAPH = 'https://graph.facebook.com/'
#FACEBOOK_OAUTH = 'https://www.facebook.com/dialog/oauth/'

API_KEY = '480727458614639'
API_SECRET = 'fd011c82c4e37bde6622fd1581d2124d'

INTERNAL_SERVER = 'http://127.0.0.1:8888'
CANVAS_PAGE = 'http://apps.facebook.com/saga-find-gift/'

FACEBOOK_GRAPH = 'https://graph.facebook.com/'
FACEBOOK_OAUTH = 'https://www.facebook.com/dialog/oauth/'
FACEBOOK_ACCESS_TOKEN = 'https://graph.facebook.com/oauth/access_token'


try:
    from local_settings import *
except ImportError:
    pass

